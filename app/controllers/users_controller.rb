class UsersController < ApplicationController

  def index
    render json: User.all
  end

  def create
    User.create user_params
    render json: { message: "Se ha creado un nuevo usuario" }, status: 200
  rescue
    render json: { message: "No se ha creado un nuevo usuario" }, status: 400
  end

  private

  def user_params
    params.permit(
      :name,
      :last_name
    )
  end
end
